let workArr = []
let workSuccess = []

const addWork = () => {
    let newWork = document.getElementById("newTask").value;
    workArr.push(newWork)
    renderAllData()

    //reset input
    document.getElementById("newTask").value = ""

}

//in công việc chưa hoàn thành
const renderTableWork = () => {
    let workHTML = ""
    for (let i = 0; i < workArr.length; i++) {
        let work = workArr[i]
        let workTR = `<li> ${work} <i class="fa fa-trash-alt" onclick="deleteIcon(${i})" style="line-height:2; color:#dddddd; font-size:15px">
        <i class="fa fa-check" onclick="check(${i})"></i></i> </li> `
        workHTML += workTR;
    }
    console.log(workHTML);
    document.getElementById("todo").innerHTML = workHTML;
}

//xoá công việc ở chưa hoàn thành
const deleteIcon = (i) => {
    workArr.splice(i, 1)
    renderTableWork()
}

//check từ chưa hoàn thành xuống hoàn thành
const check = (i) => {
    workSuccess.push(workArr[i])
    renderAllData()
}

//in table ở hoàn thành
const renderTableWorkSuccess = () => {
    let workHTML = ""
    for (let i = 0; i < workSuccess.length; i++) {
        let work = workSuccess[i]
        let workTR = `<li> ${work} <i class="fa fa-trash-alt" onclick="deleteSticker(${i})" style="line-height:2; color:#dddddd; font-size:15px">
        <i class="fa fa-check" onclick="recheck(${i})" style="color:green;"></i></i> </li> `
        workHTML += workTR;
    }
    console.log(workHTML);
    document.getElementById("completed").innerHTML = workHTML;
}

// xoá ở hoàn thành
const deleteSticker = (i) => {
    workSuccess.splice(i, 1)
    renderTableWorkSuccess()
}
//recheck từ hoàn thành lên chưa hoàn thành
const recheck = (i) => {
    workArr.push(workSuccess[i])
    renderAllData()
}
//render tổng
const renderAllData = () => {
    renderTableWorkSuccess()
    renderTableWork()
}

//sắp xếp a-z 
const sapXepADenZ = () => {
    let newWork = document.getElementById("newTask").value;
    workArr.sort()
    workSuccess.sort()
    renderAllData()
}

//sắp xếp z-a
const sapXepZDenA = () => {
    let newWork = document.getElementById("newTask").value;
    workArr.reverse()
    workSuccess.reverse()
    renderAllData()
}
